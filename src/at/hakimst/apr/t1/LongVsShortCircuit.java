package at.hakimst.apr.t1;

public class LongVsShortCircuit {

    public static void main(String[] args) {
        System.out.println("Long circuit AND:");
        boolean slong = returnFalse() & returnTrue();

        System.out.println("Short circuit AND:");
        boolean sshort = returnFalse() && returnTrue();

        System.out.println("Long circuit OR:");
        boolean rlong = returnTrue() | returnFalse();

        System.out.println("Short circuit OR:");
        boolean rshort = returnTrue() || returnFalse();


    }

    public static boolean returnTrue(){
        System.out.println("Return true call");
        return true;
    }

    public static boolean returnFalse(){
        System.out.println("Return false call");
        return false;
    }
}
