package at.hakimst.apr.t1;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DoubleVsBigDecimalExample {

    public static void main(String[] args) {
        //createSumDouble();
        createSumBigDecimal();
    }

    public static void createSumDouble() {
        double s = 0.2d;
        for(int i = 0; i < 100; i++){
            s += 0.2;
        }
        System.out.println(s);
    }

    public static void createSumBigDecimal(){
        BigDecimal b = new BigDecimal(0.2);
        b = b.setScale(10, RoundingMode.HALF_UP);
        for(int i = 0; i < 100; i++){
            b = b.add(new BigDecimal(0.2));
        }
        System.out.println(b);
    }


}
