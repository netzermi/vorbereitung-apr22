package at.hakimst.apr.t1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Loops {

    public static void main(String[] args) {
        forExamples();
    }

    public static void whileLoops(){
        int i = 0;
        while(i > 0){
            System.out.println("Call while");
            i--;
        }
        i = 0;
        do{
            System.out.println("Call do-while");
            i--;
        } while(i >0);

    }

    public static void forExamples(){
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(4);
        arrayList.add(2);
        arrayList.add(7);
        arrayList.add(9);
        //this version does not work here
        // Arrays.asList(4,2,7,9);
        //classic variant
        System.out.println("classic variant");
        for(int i = 0; i < arrayList.size(); i++){
            System.out.println(arrayList.get(i));
        }

        //for-each variant
        System.out.println("for-each variant");
        for(Integer i : arrayList){
//            if(i.equals(2)){
//                arrayList.remove(i);
//            }
            System.out.println(i);
        }

        //iterator variant
        System.out.println("iterator variant");
        Iterator<Integer> iterator = arrayList.iterator();
        while(iterator.hasNext()){
            Integer i = iterator.next();
            if(i.equals(2)){
                iterator.remove();
            } else {
                System.out.println(i);
            }
        }
        System.out.println("Iterator array list final: " + arrayList);

        //lambda variant
        System.out.println("lambda variant");
        arrayList.forEach(System.out::println);
    }
}
