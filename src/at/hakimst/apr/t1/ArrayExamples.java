package at.hakimst.apr.t1;

import java.util.Arrays;

public class ArrayExamples {

    public static void main(String[] args) {
        String line = "3;3;2";
        String[] elements = line.split(";");
        System.out.println(Arrays.toString(elements));

        int[] a = new int[3];
        a[0] = 4;
        a[1] = 2;
        int[] a2 = {4,2,7};

        int[][] matrix = {{1,2,3},{4,5,6}};
        System.out.println(matrix.length);
        System.out.println(matrix[0].length);
        for(int r = 0; r < matrix.length; r++){
            for(int c = 0; c < matrix[0].length; c++){
                System.out.print(matrix[r][c] + " ");
            }
            System.out.println();
        }


    }

}
