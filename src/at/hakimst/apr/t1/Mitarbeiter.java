package at.hakimst.apr.t1;

import java.util.UUID;

public class Mitarbeiter{
    private UUID id;
    private String vorname;
    private String nachname;
    private double umsatz;

    public Mitarbeiter(UUID id, String vorname, String nachname, double umsatz){
        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
        this.umsatz = umsatz;
    }

    public void umsatzVermehren(){
        double faktor = 2d;
        umsatz *= faktor;
    }



}
